import { flatMap } from 'lodash';

export const getPokeColor = item => {
  const types = flatMap(item.types, ({ type }) => type.name);

  if (types.includes('grass')) return '#49d0b1';
  else if (types.includes('fairy')) return '#ffa9a9';
  else if (types.includes('electric')) return '#fdcd4b';
  else if (types.includes('fire')) return '#fb6b6b';
  else if (types.includes('water')) return '#7ac7ff';
  else if (types.includes('bug')) return '#48d0b0';
  else if (types.includes('ground')) return '#bda46b';
  else return '#b1736c';
};
