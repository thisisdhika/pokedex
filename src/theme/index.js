import { extendTheme } from '@chakra-ui/react';
import { mode, createBreakpoints } from '@chakra-ui/theme-tools';

const breakpoints = createBreakpoints({
  xs: '375px',
  sm: '576px',
  md: '768px',
  lg: '992px',
  xl: '1200px',
  '2xl': '1400px',
});

const numericStyles = {
  '&[data-is-numeric=true]': {
    textAlign: 'end',
  },
};

const theme = extendTheme({
  breakpoints,
  sizes: {
    container: {
      xs: '415px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
    },
  },
  components: {
    Button: {
      baseStyle: {
        '&:focus': {
          boxShadow: 'unset',
        },
      },
    },
    Tabs: {
      parts: ['root', 'tablist', 'tab'],
      defaultProps: {
        colorScheme: 'blackAlpha',
      },
      variants: {
        pokeTab: props => {
          const { colorScheme: c, orientation } = props;
          const isVertical = orientation === 'vertical';
          const borderProp =
            orientation === 'vertical' ? 'borderStart' : 'borderBottom';
          const marginProp = isVertical ? 'marginStart' : 'marginBottom';

          return {
            tabpanel: {
              pb: 0,
            },
            tablist: {
              [borderProp]: '2px solid',
              borderColor: 'inherit',
            },
            tab: {
              [borderProp]: '2px solid',
              borderColor: 'transparent',
              [marginProp]: '-2px',
              fontWeight: 'black',
              color: mode(`${c}.500`, `${c}.900`)(props),
              fontSize: 'sm',
              _selected: {
                color: mode(`${c}.900`, `${c}.500`)(props),
                borderColor: 'currentColor',
              },
              _active: {
                bg: 'whiteAlpha.300',
              },
              _disabled: {
                opacity: 0.4,
                cursor: 'not-allowed',
              },
              _focus: {
                boxShadow: 'unset',
              },
            },
          };
        },
      },
    },
    Table: {
      variants: {
        pokeTable: props => ({
          th: {
            px: 0.5,
            py: 2.5,
            fontSize: 'md',
            fontWeight: 'bold',
            textTransform: 'capitalize',
            color: mode(`gray.500`, `gray.200`)(props),
            ...numericStyles,
          },
          td: {
            py: 2.5,
            fontWeight: 'black',
            color: mode(`blackAlpha.800`, `whiteAlpha.800`)(props),
            ...numericStyles,
          },
          caption: {
            color: mode(`gray.600`, `gray.100`)(props),
          },
          tfoot: {
            tr: {
              '&:last-of-type': {
                th: { borderBottomWidth: 0 },
              },
            },
          },
        }),
      },
    },
  },
});

export default theme;
