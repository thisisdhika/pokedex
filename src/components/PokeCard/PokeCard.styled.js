import styled from '@emotion/styled';
import pokeballImg from '../../assets/images/pokeball-halftrans.png';

const StyledPokeCard = styled('div', {
  shouldForwardProp: false,
})`
  margin: ${({ theme }) => theme.space[1]};
  width: ${({ width, theme }) =>
    !!width ? `calc(${width} - (${theme.space[1]} * 2))` : 'auto'};
  background-image: url(${pokeballImg});
  background-size: 110px;
  background-repeat: no-repeat;
  background-position: 100px 75px;
  background-color: ${({ bgColor, theme }) =>
    !!bgColor ? bgColor : theme.colors.gray[300]};
  padding-top: ${({ theme }) => theme.space[4]};
  padding-bottom: ${({ theme }) => theme.space[4]};
  padding-left: ${({ theme }) => theme.space[5]};
  padding-right: ${({ theme }) => theme.space[5]};
  border-radius: ${({ theme }) => theme.radii['3xl']};
  min-height: ${({ theme }) => theme.sizes[40]};
  transition-duration: ${({ theme }) => theme.transition.duration.slow};
  transition-property: ${({ theme }) => theme.transition.property.background};
  transition-timing-function: ${({ theme }) =>
    theme.transition.easing['ease-out']};
`;

StyledPokeCard.passProps = false;

export default StyledPokeCard;
