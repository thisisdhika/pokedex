import React from 'react';
import { Tag } from '@chakra-ui/tag';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { has, isEmpty, isUndefined } from 'lodash';
import { Center } from '@chakra-ui/layout';
import { bindActionCreators } from 'redux';
import { Spinner } from '@chakra-ui/spinner';
import { pokemonActions } from '../../store/actions';
import { useInView } from 'react-intersection-observer';
import { Box, Grid, Heading, VStack } from '@chakra-ui/layout';
import { CSSTransition, SwitchTransition } from 'react-transition-group';

// Styled
import StyledPokeCard from './PokeCard.styled';

const PokeCard = ({ url, id, pokemon, getSinglePokemon, ...rest }) => {
  const data = pokemon[id];
  const pokemonData = has(data, 'data') ? data.data : {};
  const [isFetched, setIsFetched] = React.useState(false);
  const isLoading = !isUndefined(data) && data.isFetching;

  const { ref, inView } = useInView({
    /* Optional options */
    threshold: 0,
  });

  React.useEffect(() => {
    if (!isFetched && inView && isEmpty(pokemonData)) {
      (async () => {
        await getSinglePokemon(url, id);
        setIsFetched(true);
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inView, isFetched, url, id]);

  return (
    <StyledPokeCard
      ref={ref}
      as={isLoading ? 'div' : Link}
      to={`/detail/${has(pokemonData, 'color') ? pokemonData.name : ''}`}
      bgColor={has(pokemonData, 'color') ? pokemonData.color : ''}
      {...rest}
    >
      <SwitchTransition>
        <CSSTransition
          classNames="fade"
          key={isLoading}
          addEndListener={(node, done) =>
            node.addEventListener('transitionend', done, false)
          }
        >
          {isLoading ? (
            <Center height="100%">
              <Spinner
                size="lg"
                speed="0.65s"
                thickness="4px"
                color="gray.600"
                emptyColor="gray.50"
              />
            </Center>
          ) : (
            <>
              <Heading
                as="h1"
                fontSize="2xl"
                textTransform="uppercase"
                color="blackAlpha.400"
                textAlign="end"
              >
                #{pokemonData.str_id}
              </Heading>
              <Heading
                as="h2"
                fontSize="xl"
                textTransform="capitalize"
                color="white"
              >
                {pokemonData.name}
              </Heading>
              <Grid templateColumns="repeat(2, 1fr)">
                <VStack py="2" alignItems="flex-start">
                  {has(pokemonData, 'types') &&
                    pokemonData.types.map((type, tk) => (
                      <Tag
                        key={tk}
                        borderRadius="full"
                        variant="solid"
                        colorScheme="whiteAlpha"
                        textTransform="capitalize"
                        px="3.5"
                      >
                        <strong>{type.type.name}</strong>
                      </Tag>
                    ))}
                </VStack>
                <Box mr="-2" mb="-2">
                  <img src={pokemonData.image_url} alt={pokemonData.name} />
                </Box>
              </Grid>
            </>
          )}
        </CSSTransition>
      </SwitchTransition>
    </StyledPokeCard>
  );
};

PokeCard.defaultProps = {};

const mapStateToProps = ({ pokemon }) => ({
  pokemon,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getSinglePokemon: pokemonActions.getPokemon }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PokeCard);
