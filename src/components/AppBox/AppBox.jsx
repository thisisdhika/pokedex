import React from 'react';
import { has, isEmpty, toArray } from 'lodash';
import { connect } from 'react-redux';
import { Box } from '@chakra-ui/layout';
import { bindActionCreators } from 'redux';
import { css, Global } from '@emotion/react';
import { pokemonActions } from '../../store/actions';
import { matchPath, useHistory, useLocation } from 'react-router';

// Styled
import StyledAppBox from './AppBox.styled';

const AppBox = ({ children, pokemon, header: Header }) => {
  const history = useHistory();
  const location = useLocation();
  const [isInDetail, setInDetail] = React.useState(false);
  const [pokemonName, setPokemonName] = React.useState(null);
  const [hasReachBottom, setHasReachBottom] = React.useState(false);
  const [shouldHeaderSticky, setShouldHeaderSticky] = React.useState(false);

  const data = toArray(pokemon).find(poke => poke.data.name === pokemonName);
  const pokemonData = has(data, 'data') ? data.data : {};

  const handleOnWrapperScroll = e => {
    const el = e.target;

    if (!shouldHeaderSticky && el.scrollTop >= 30) setShouldHeaderSticky(true);
    else if (shouldHeaderSticky && el.scrollTop < 30)
      setShouldHeaderSticky(false);

    setHasReachBottom(el.scrollTop + el.clientHeight >= el.scrollHeight - 150);
  };

  React.useEffect(() => {
    const routeListener = location => {
      const matched = matchPath(location.pathname, {
        path: '/detail/:pokemonName',
      });

      const hasPokemonName = !!matched && has(matched.params, 'pokemonName');

      setInDetail(!!hasPokemonName);
      if (hasPokemonName) setPokemonName(matched.params.pokemonName);
    };

    routeListener(location);
    history.listen(routeListener);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [history]);

  React.useEffect(() => {
    if (isInDetail && isEmpty(pokemonData)) history.push('/');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isInDetail]);

  return (
    <React.Fragment>
      <Global
        styles={css`
          body {
            background-color: #e2e2e2;
          }
          .fade-enter {
            opacity: 0;
          }
          .fade-exit {
            opacity: 1;
          }
          .fade-enter-active {
            opacity: 1;
          }
          .fade-exit-active {
            opacity: 0;
          }
          .fade-enter-active,
          .fade-exit-active {
            transition: opacity 350ms;
          }
        `}
      />
      <StyledAppBox
        isInDetail={isInDetail}
        hasReachBottom={hasReachBottom}
        bgColor={
          isInDetail && has(pokemonData, 'color') ? pokemonData.color : ''
        }
      >
        <Box className="wrapper" onScroll={handleOnWrapperScroll}>
          <Header
            colorScheme={isInDetail ? 'dark' : 'light'}
            isSticky={shouldHeaderSticky}
          />
          <Box className="content">{children}</Box>
        </Box>
      </StyledAppBox>
    </React.Fragment>
  );
};

AppBox.defaultProps = {};

const mapStateToProps = ({ pokemon }) => ({
  pokemon,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getSinglePokemon: pokemonActions.getPokemon }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AppBox);
