import styled from '@emotion/styled';

// Assets
import pokeballImg from '../../assets/images/pokeball-halftrans.png';

const StyledAppBox = styled.div`
  position: fixed;
  overflow: hidden;
  box-shadow: ${({ theme }) => theme.shadows['xl']};
  background-image: url(${pokeballImg});
  background-color: ${({ theme, bgColor }) =>
    !!bgColor ? bgColor : theme.colors.white};
  background-size: ${({ theme, isInDetail }) =>
    isInDetail ? theme.sizes[48] : theme.sizes.xs};
  background-position-x: ${({ isInDetail }) => (isInDetail ? 200 : 175)}px;
  background-position-y: ${({ isInDetail }) => (isInDetail ? 200 : -60)}px;
  background-repeat: no-repeat;
  transition-duration: ${({ theme }) => theme.transition.duration.slow};
  transition-property: background-color, background-size, background-position-x,
    background-position-y;
  transition-timing-function: ${({ theme }) =>
    theme.transition.easing['ease-out']};

  .wrapper {
    overflow-y: ${({ isInDetail }) => (isInDetail ? 'hidden' : 'auto')};
    overflow-x: hidden;
    scrollbar-color: white green;
    scrollbar-width: thin;
    padding-top: 125px;
    max-height: 100vh;
    min-height: 100vh;
    max-width: 100vw;
    min-width: 100vw;
    display: flex;

    &::-webkit-scrollbar {
      width: 0px;
    }

    &::-webkit-scrollbar-track {
      background: green;
    }

    &::-webkit-scrollbar-thumb {
      background-color: white;
      border-radius: 20px;
      border: 3px solid green;
    }

    &::after {
      content: '';
      position: absolute;
      bottom: ${({ hasReachBottom, isInDetail, theme }) =>
        hasReachBottom || isInDetail ? `calc(-1 * ${theme.sizes[32]})` : 0};
      left: 0;
      right: 0;
      width: 100%;
      height: ${({ theme }) => theme.sizes[32]};
      pointer-events: none;
      background: rgb(255, 255, 255);
      background: linear-gradient(
        180deg,
        rgba(255, 255, 255, 0) 0%,
        rgba(255, 255, 255, 0.65) 50%,
        rgba(255, 255, 255, 1) 100%
      );
      transition-property: bottom;
      transition-duration: ${({ theme }) => theme.transition.duration.slow};
      transition-timing-function: ${({ theme }) =>
        theme.transition.easing['ease-out']};
    }

    .content {
      flex: 1;
      display: flex;
      min-height: calc(100vh - 170px);

      & > * {
        width: 100%;
      }
    }
  }

  @media screen and (min-width: ${({ theme }) => theme.breakpoints.sm}) {
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    border-radius: ${({ theme }) => theme.radii['3xl']};
    min-width: ${({ theme }) => theme.sizes.container.xs};
    max-width: ${({ theme }) => theme.sizes.container.xs};
    background-position-x: ${({ isInDetail }) => (isInDetail ? 245 : 215)}px;

    .wrapper {
      max-height: calc(100vh - 45px);
      min-height: calc(100vh - 45px);
      max-width: unset;
      min-width: unset;
    }
  }
`;

export default StyledAppBox;
