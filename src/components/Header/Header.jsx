import { IconButton } from '@chakra-ui/button';
import { FaArrowLeft, FaList } from 'react-icons/fa';
import { Link, useLocation } from 'react-router-dom';
import { Container, Flex, Heading } from '@chakra-ui/layout';
import { CSSTransition, SwitchTransition } from 'react-transition-group';

// Styled
import StyledHeader from './Header.styled';

const Header = ({ title, colorScheme, isSticky }) => {
  const location = useLocation();

  return (
    <StyledHeader colorScheme={colorScheme} isSticky={isSticky}>
      <Container>
        <Flex justifyContent="space-between">
          <SwitchTransition>
            <CSSTransition
              classNames="fade"
              key={location.pathname === '/'}
              addEndListener={(node, done) =>
                node.addEventListener('transitionend', done, false)
              }
            >
              {location.pathname === '/' ? (
                <Heading
                  pl={1}
                  as="h1"
                  display="flex"
                  fontSize="3xl"
                  alignItems="center"
                >
                  {title}
                </Heading>
              ) : (
                <IconButton
                  as={Link}
                  to="/"
                  size="lg"
                  fontSize="2xl"
                  variant="ghost"
                  icon={<FaArrowLeft />}
                  colorScheme={
                    colorScheme === 'dark' ? 'whiteAlpha.800' : 'blackAlpha.800'
                  }
                />
              )}
            </CSSTransition>
          </SwitchTransition>
          <IconButton
            variant="ghost"
            aria-label="Menu"
            size="lg"
            fontSize="2xl"
            icon={<FaList />}
            colorScheme={
              colorScheme === 'dark' ? 'whiteAlpha.800' : 'blackAlpha.800'
            }
          />
        </Flex>
      </Container>
    </StyledHeader>
  );
};

Header.defaultProps = {
  title: 'Pokedex',
};

export default Header;
