import styled from '@emotion/styled';

const StyledHeader = styled.div`
  position: absolute;
  padding-bottom: 15px;
  padding-top: ${({ isSticky }) => `${isSticky ? 15 : 75}px`};
  width: 100%;
  top: 0;
  left: 0;
  right: 0;
  transition-duration: ${({ theme }) => theme.transition.duration.slow};
  transition-property: ${({ theme }) =>
    `${theme.transition.property.background}, padding-top`};
  transition-timing-function: ${({ theme }) =>
    theme.transition.easing['ease-out']};
  background-color: ${({ isSticky, theme }) =>
    isSticky ? theme.colors.white : 'transparent'};
  box-shadow: ${({ isSticky, theme }) =>
    isSticky ? theme.shadows['xl'] : 'unset'};
  z-index: ${({ theme }) => theme.zIndices.sticky};
  color: ${({ theme, colorScheme }) =>
    colorScheme === 'dark' ? theme.colors.white : theme.colors.black};
`;

export default StyledHeader;
