import React from 'react';
import { PokeCard } from '../../components';
import { Spinner } from '@chakra-ui/spinner';
import { Center, Flex } from '@chakra-ui/layout';
import { Box, Container } from '@chakra-ui/layout';
import { CSSTransition, SwitchTransition } from 'react-transition-group';

const Home = ({ pokedex, getPokedex }) => {
  React.useEffect(() => {
    if (!pokedex.data.length) getPokedex();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pokedex.data]);

  return (
    <Box py={5}>
      <Container>
        <SwitchTransition>
          <CSSTransition
            classNames="fade"
            key={pokedex.isFetching}
            addEndListener={(node, done) =>
              node.addEventListener('transitionend', done, false)
            }
          >
            {pokedex.isFetching ? (
              <Center minH="70vh">
                <Spinner
                  size="xl"
                  speed="0.65s"
                  thickness="4px"
                  color="gray.600"
                  emptyColor="gray.50"
                />
              </Center>
            ) : (
              <Flex flexWrap="wrap" pb={5}>
                {pokedex.data.map((pokemon, key) => (
                  <PokeCard
                    key={key}
                    url={pokemon.url}
                    id={key + 1}
                    width="50%"
                  />
                ))}
              </Flex>
            )}
          </CSSTransition>
        </SwitchTransition>
      </Container>
    </Box>
  );
};

Home.defaultProps = {
  pokedex: {
    data: [],
  },
};

export default Home;
