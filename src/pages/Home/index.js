import Home from './Home';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { pokedexActions } from '../../store/actions';

const mapStateToProps = ({ pokedex }) => ({
  pokedex,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getPokedex: pokedexActions.getPokedex }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
