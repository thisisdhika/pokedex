import React from 'react';
import { Tag } from '@chakra-ui/tag';
import { Tab } from '@chakra-ui/tabs';
import { Tabs } from '@chakra-ui/tabs';
import { useParams } from 'react-router';
import { Flex, Text } from '@chakra-ui/layout';
import { TabList } from '@chakra-ui/tabs';
import { HStack } from '@chakra-ui/layout';
import { TabPanel } from '@chakra-ui/tabs';
import { TabPanels } from '@chakra-ui/tabs';
import { has, isEmpty, keys, toArray } from 'lodash';
import { Box, Container, Heading } from '@chakra-ui/layout';
import { Table, Tbody, Td, Th, Tr } from '@chakra-ui/table';
import { FaMars, FaVenus } from 'react-icons/fa';
import Icon from '@chakra-ui/icon';
import { Progress } from '@chakra-ui/progress';

const Detail = ({ pokemon }) => {
  const { pokemonName } = useParams();
  const data = toArray(pokemon).find(({ data }) => data.name === pokemonName);
  const pokemonData = has(data, 'data') ? data.data : {};
  // console.log('pokemonData: ', pokemonData);

  const tabList = {
    about: {
      label: 'About',
      content: (
        <Box>
          <Table mb={3} variant="pokeTable">
            <Tbody>
              <Tr>
                <Th w={32}>Species</Th>
                <Td>Seed</Td>
              </Tr>
              <Tr>
                <Th w={32}>Height</Th>
                <Td>2'3,6" (0.70cm)</Td>
              </Tr>
              <Tr>
                <Th w={32}>Weight</Th>
                <Td>15.2 lbs (6,9 kg)</Td>
              </Tr>
              <Tr>
                <Th w={32}>Abilities</Th>
                <Td>Overgrow, Chlorophyl</Td>
              </Tr>
            </Tbody>
          </Table>
          <Heading as="h1" size="md">
            Breeding
          </Heading>
          <Table mt={1} variant="pokeTable">
            <Tbody>
              <Tr>
                <Th w={32}>Gender</Th>
                <Td>
                  <Box display="flex" alignItems="center" as="span">
                    <Icon as={FaMars} color="blue.400" boxSize="1.5em" mr={1} />
                    87.5%
                    <Icon
                      as={FaVenus}
                      color="pink.400"
                      boxSize="1.25em"
                      ml={3}
                      mr={1}
                    />
                    12.5%
                  </Box>
                </Td>
              </Tr>
              <Tr>
                <Th w={32}>Egg Groupd</Th>
                <Td>Monster</Td>
              </Tr>
              <Tr>
                <Th w={32}>Egg Cycle</Th>
                <Td>Grass</Td>
              </Tr>
            </Tbody>
          </Table>
        </Box>
      ),
    },
    base_stats: {
      label: 'Base Stats',
      content: (
        <Box>
          <Table mb={4} variant="pokeTable">
            <Tbody>
              <Tr>
                <Th>HP</Th>
                <Td w={1} textAlign="center">
                  45
                </Td>
                <Td w={48} px={0}>
                  <Progress size="xs" colorScheme="red" value={45} />
                </Td>
              </Tr>
              <Tr>
                <Th>Attack</Th>
                <Td w={1} textAlign="center">
                  60
                </Td>
                <Td w={48} px={0}>
                  <Progress size="xs" colorScheme="green" value={60} />
                </Td>
              </Tr>
              <Tr>
                <Th>Defense</Th>
                <Td w={1} textAlign="center">
                  48
                </Td>
                <Td w={48} px={0}>
                  <Progress size="xs" colorScheme="red" value={48} />
                </Td>
              </Tr>
              <Tr>
                <Th>Sp. Atk</Th>
                <Td w={1} textAlign="center">
                  65
                </Td>
                <Td w={48} px={0}>
                  <Progress size="xs" colorScheme="green" value={65} />
                </Td>
              </Tr>
              <Tr>
                <Th>Sp. Def</Th>
                <Td w={1} textAlign="center">
                  65
                </Td>
                <Td w={48} px={0}>
                  <Progress size="xs" colorScheme="green" value={65} />
                </Td>
              </Tr>
              <Tr>
                <Th>Speed</Th>
                <Td w={1} textAlign="center">
                  45
                </Td>
                <Td w={48} px={0}>
                  <Progress size="xs" colorScheme="red" value={45} />
                </Td>
              </Tr>
              <Tr>
                <Th>Total</Th>
                <Td w={1} textAlign="center">
                  317
                </Td>
                <Td w={48} px={0}>
                  <Progress
                    size="xs"
                    max={600}
                    colorScheme="green"
                    value={317}
                  />
                </Td>
              </Tr>
            </Tbody>
          </Table>
          <Heading as="h1" size="md">
            Type Defenses
          </Heading>
          <Text mt={1} color="gray.600">
            The effectiveness of each type of {pokemonName}
          </Text>
        </Box>
      ),
    },
    evolution: {
      label: 'Evolution',
      content: (
        <Box>
          <Text mt={1} color="gray.600">
            Et Lorem irure labore eu id culpa velit enim ad consectetur non
            commodo culpa. Magna nisi eiusmod ad esse adipisicing pariatur esse
            velit do. Tempor enim in sit incididunt proident sit aute consequat
            enim excepteur elit exercitation aliquip amet. Mollit laborum
            consequat non incididunt consectetur. Ex proident aliquip dolor
            dolor duis dolore sunt amet. Id qui ad pariatur nisi veniam.
          </Text>
        </Box>
      ),
    },
    moves: {
      label: 'Moves',
      content: (
        <Box>
          <Text mt={1} color="gray.600">
            Minim ullamco officia quis culpa ut qui Lorem. Laboris pariatur
            consectetur minim ex ad mollit dolor amet proident ut aliqua. Nulla
            do voluptate nostrud irure voluptate cillum excepteur. Anim
            excepteur reprehenderit aliqua elit laborum proident consectetur. Id
            consectetur reprehenderit cupidatat anim fugiat. Anim deserunt aute
            et id commodo duis ad minim minim aliquip aliqua cillum mollit.
          </Text>
        </Box>
      ),
    },
  };

  return (
    !isEmpty(pokemonData) && (
      <Flex pt={0.5}>
        <Flex flex="1" flexDirection="column">
          <Container>
            <Flex px={3} alignItems="center">
              <Box flex="1">
                <Heading
                  as="h1"
                  color="white"
                  fontSize="4xl"
                  textTransform="capitalize"
                >
                  {pokemonName}
                </Heading>
                <HStack py={1} alignItems="flex-start">
                  {has(pokemonData, 'types') &&
                    pokemonData.types.map((type, tk) => (
                      <Tag
                        key={tk}
                        borderRadius="full"
                        variant="solid"
                        colorScheme="whiteAlpha"
                        textTransform="capitalize"
                        px="3.5"
                      >
                        <strong>{type.type.name}</strong>
                      </Tag>
                    ))}
                </HStack>
              </Box>
              <Heading
                mt={4}
                as="span"
                color="white"
                fontSize="xl"
                textTransform="uppercase"
              >
                <strong>#{pokemonData.str_id}</strong>
              </Heading>
            </Flex>
          </Container>
          <Box pt={2.5} position="relative" zIndex="banner">
            <Box
              as="img"
              m="auto"
              maxH={44}
              src={pokemonData.image_url}
              alt={pokemonData.name}
            />
          </Box>
          <Box
            flex="1"
            bgColor="white"
            roundedTop="3xl"
            mt="-12"
            pt={10}
            px={3}
          >
            <Container>
              <Tabs isFitted variant="pokeTab">
                <TabList>
                  {keys(tabList).map(key => (
                    <Tab
                      px={0}
                      key={key}
                      minW={key === 'base_stats' ? 28 : 'unset'}
                    >
                      {tabList[key].label}
                    </Tab>
                  ))}
                </TabList>

                <TabPanels>
                  {keys(tabList).map(key => (
                    <TabPanel px={0} key={key}>
                      {tabList[key].content}
                    </TabPanel>
                  ))}
                </TabPanels>
              </Tabs>
            </Container>
          </Box>
        </Flex>
      </Flex>
    )
  );
};

Detail.defaultProps = {};

export default Detail;
