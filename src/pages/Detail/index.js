import Detail from './Detail';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const mapStateToProps = ({ pokemon }) => ({
  pokemon,
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
