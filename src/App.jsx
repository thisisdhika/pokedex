import theme from './theme';
import { ChakraProvider } from '@chakra-ui/react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { AppBox, Header } from './components';
import { Home, Detail } from './pages';

const App = () => {
  return (
    <ChakraProvider theme={theme}>
      <BrowserRouter>
        <Switch>
          <AppBox header={props => <Header {...props} />}>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/detail/:pokemonName">
              <Detail />
            </Route>
          </AppBox>
        </Switch>
      </BrowserRouter>
    </ChakraProvider>
  );
};

export default App;
