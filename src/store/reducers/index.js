import { combineReducers } from 'redux';

import pokedex from './pokedex';
import pokemon from './pokemon';

const rootReducer = combineReducers({
  pokedex,
  pokemon,
});

export default rootReducer;
