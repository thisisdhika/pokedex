import { FETCH_POKEDEX } from '../constants';

const INITIAL_STATE = {
  data: [],
  count: 0,
  limit: 20,
  offset: 0,
  next: null,
  previous: null,
  isFetching: true,
  fetchError: null,
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_POKEDEX.BEGIN:
      return {
        ...state,
        isFetching: true,
      };

    case FETCH_POKEDEX.SUCCESS:
      const { results, count, next, previous } = action.data;

      return {
        ...state,
        isFetching: false,
        fetchError: null,
        data: results,
        count: count,
        next: next,
        previous: previous,
      };

    case FETCH_POKEDEX.FAILURE:
      return {
        ...state,
        isFetching: false,
        fetchError: action.error,
      };

    default:
      return state;
  }
};

export default reducer;
