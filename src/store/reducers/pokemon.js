import { has } from 'lodash';
import { FETCH_POKEMON } from '../constants';

const INITIAL_ITEM_STATE = {
  data: {},
  isFetching: true,
  fetchError: null,
};

const INITIAL_STATE = {};

const reducer = (state = INITIAL_STATE, { type, id, ...restAction }) => {
  switch (type) {
    case FETCH_POKEMON.BEGIN:
      if (has(state, id)) {
        return {
          ...state,
          [id]: {
            ...state[id],
            isFetching: true,
          },
        };
      }

      return {
        ...state,
        [id]: INITIAL_ITEM_STATE,
      };

    case FETCH_POKEMON.SUCCESS:
      return {
        ...state,
        [id]: {
          ...state[id],
          isFetching: false,
          fetchError: null,
          data: restAction.data,
        },
      };

    case FETCH_POKEMON.FAILURE:
      return {
        ...state,
        [id]: {
          ...state[id],
          isFetching: false,
          fetchError: restAction.error,
        },
      };

    default:
      return state;
  }
};

export default reducer;
