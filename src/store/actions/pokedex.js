import axios from 'axios';
import { FETCH_POKEDEX } from '../constants';

export const dispatchGetPokedexBegin = () => ({
  type: FETCH_POKEDEX.BEGIN,
});

export const dispatchGetPokedexSuccess = data => ({
  data,
  type: FETCH_POKEDEX.SUCCESS,
});

export const dispatchGetPokedexError = error => ({
  error,
  type: FETCH_POKEDEX.FAILURE,
});

export const getPokedex = params => async (dispatch, getState) => {
  const currState = getState().pokedex;

  dispatch(dispatchGetPokedexBegin());

  try {
    const { data } = await axios.get('https://pokeapi.co/api/v2/pokemon', {
      params: {
        ...params,
        limit: currState.limit,
        offset: currState.offset,
      },
    });

    setTimeout(() => {
      dispatch(dispatchGetPokedexSuccess(data));
    }, 2500);

    return data;
  } catch (error) {
    console.error('[Pokedex] Pokedex Reducer: ', error);

    dispatch(dispatchGetPokedexError(error));

    return Promise.reject(error);
  }
};
