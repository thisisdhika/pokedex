import axios from 'axios';
import { FETCH_POKEMON } from '../constants';
import { getPokeColor } from '../../helpers/utils';

export const dispatchGetPokemonBegin = id => ({
  id,
  type: FETCH_POKEMON.BEGIN,
});

export const dispatchGetPokemonSuccess = (id, data) => ({
  id,
  data,
  type: FETCH_POKEMON.SUCCESS,
});

export const dispatchGetPokemonError = (id, error) => ({
  id,
  error,
  type: FETCH_POKEMON.FAILURE,
});

export const getPokemon = (url, id) => async dispatch => {
  dispatch(dispatchGetPokemonBegin(id));

  try {
    const res = await axios.get(url);

    const pad = '000';
    const numStr = res.data.id.toString();
    const strId = pad.substring(0, pad.length - numStr.length) + numStr;

    const data = {
      ...res.data,
      str_id: strId,
      image_url: `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${strId}.png`,
      color: getPokeColor(res.data),
    };

    setTimeout(() => {
      dispatch(dispatchGetPokemonSuccess(id, data));
    }, 1500);

    return data;
  } catch (error) {
    console.error('[Pokedex] Pokemon Reducer: ', error);

    dispatch(dispatchGetPokemonError(id, error));

    return Promise.reject(error);
  }
};
