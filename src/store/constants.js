const makeCommonConstant = type => ({
  BEGIN: type.concat('_BEGIN'),
  SUCCESS: type.concat('_SUCCESS'),
  FAILURE: type.concat('_FAILURE'),
});

export const FETCH_POKEDEX = makeCommonConstant('FETCH_POKEDEX');

export const FETCH_POKEMON = makeCommonConstant('FETCH_POKEMON');
